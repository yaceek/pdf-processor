package com.itartisan.multitool.pdf.processors;

import com.itextpdf.kernel.pdf.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class PdfSecurityProcessor {
    public static byte[] process(byte[] source, String password) throws IOException {
        try (var output = new ByteArrayOutputStream()) {
            try (var input = new ByteArrayInputStream(source)) {
                var props = new WriterProperties().setStandardEncryption(password.getBytes(StandardCharsets.UTF_8), null, EncryptionConstants.ALLOW_PRINTING | EncryptionConstants.ALLOW_DEGRADED_PRINTING | EncryptionConstants.ALLOW_SCREENREADERS, EncryptionConstants.ENCRYPTION_AES_128 | EncryptionConstants.DO_NOT_ENCRYPT_METADATA);
                var pdf = new PdfDocument(new PdfReader(input), new PdfWriter(output, props));
                pdf.close();
            }
            return output.toByteArray();
        }
    }
}
