package com.itartisan.multitool.pdf.processors;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.utils.PdfMerger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class PdfMergerProcessor {
    public static byte[] merge(List<byte[]> sources) throws IOException {
        try (var output = new ByteArrayOutputStream()) {
            var target = new PdfDocument(new PdfWriter(output));
            var merger = new PdfMerger(target);

            merger.setCloseSourceDocuments(true);
            for (var source : sources) {
                try (var input = new ByteArrayInputStream(source)) {
                    var src = new PdfDocument(new PdfReader(input));
                    merger.merge(src, 1, src.getNumberOfPages());
                }
            }
            target.close();

            return output.toByteArray();
        }
    }
}
