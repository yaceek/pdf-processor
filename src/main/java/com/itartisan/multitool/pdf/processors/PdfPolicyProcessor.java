package com.itartisan.multitool.pdf.processors;

import com.itartisan.multitool.pdf.loaders.FileLoader;
import com.itartisan.multitool.pdf.tags.QRCodeTagCssApplierFactory;
import com.itartisan.multitool.pdf.tags.QRCodeTagWorkerFactory;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class PdfPolicyProcessor {
    public static byte[] process(String dir, String file, Map<String, String> parameters, Map<String, String[]> transforms, List<String> attachments) throws IOException {
        var template = FileLoader.loadTemplate(dir, file);

        for (int i = 0; i < attachments.size(); i++) {
            parameters.put("zalacznik_" + (i + 1), attachments.get(i));
        }

        template = interpolate(template, parameters, transforms);

        try (var output = new ByteArrayOutputStream()) {
            var properties = new ConverterProperties().setBaseUri(FileLoader.dir(dir)).setTagWorkerFactory(new QRCodeTagWorkerFactory()).setCssApplierFactory(new QRCodeTagCssApplierFactory());
            HtmlConverter.convertToPdf(template, output, properties);
            return output.toByteArray();
        }
    }

    private static String interpolate(String template, Map<String, String> parameters, Map<String, String[]> transforms) {
        var matcher = Pattern.compile("\\{\\w+(\\|\\w+)?}").matcher(template);
        var matches = new ArrayList<String>();
        while (matcher.find()) {
            matches.add(matcher.group(0));
        }

        for (var match : matches) {
            var mark = StringUtils.split(match, "|{}");
            var hide = mark[0].startsWith("ukryj_");
            var line = mark[0].startsWith("kreska_");
            var eq = mark[0].startsWith("rowne_");

            var val = parameters.get(mark[0]);
            val = hide ? parameters.get(mark[0].substring(6)) : val;
            val = line ? parameters.get(mark[0].substring(7)) : val;
            val = eq ? parameters.get(mark[0].substring(6, mark[0].lastIndexOf('_'))) : val;
            if (val == null) {
                val = "";
            }

            if (eq && !val.equals(mark[0].substring(mark[0].lastIndexOf('_') + 1))) {
                template = template.replace(match, "hide");
            } else if (hide && val.isEmpty()) {
                template = template.replace(match, "hide");
            } else if (line && val.isEmpty()) {
                template = template.replace(match, "—");
            } else if (mark.length > 1) {
                var transform = transforms.get(mark[1]);
                template = template.replace(match, val.replaceAll(transform[0], transform[1]));
            } else {
                template = template.replace(match, val);
            }
        }
        return template;
    }
}
