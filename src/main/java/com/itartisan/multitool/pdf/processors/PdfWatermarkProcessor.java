package com.itartisan.multitool.pdf.processors;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PdfWatermarkProcessor {
    public static byte[] process(byte[] source) throws IOException {
        try (var output = new ByteArrayOutputStream()) {
            try (var input = new ByteArrayInputStream(source)) {
                var pdf = new PdfDocument(new PdfReader(input), new PdfWriter(output));
                var doc = new Document(pdf);
                for (int i = 1; i <= pdf.getNumberOfPages(); i++) {
                    var page = pdf.getPage(i);
                    var x = (page.getPageSize().getLeft() + page.getPageSize().getRight()) / 2;
                    var y = (page.getPageSize().getTop() + page.getPageSize().getBottom()) / 2;

                    doc.showTextAligned(new Paragraph("D E M O   D E M O   D E M O").setFontSize(70.0f).setOpacity(Float.parseFloat(System.getProperty("multitool.watermark.opacity", "0.1"))), x, y, i, TextAlignment.CENTER, VerticalAlignment.MIDDLE, 0.97f);
                }
                doc.close();
            }
            return output.toByteArray();
        }
    }
}
