package com.itartisan.multitool.pdf.loaders;

import com.itartisan.multitool.rest.utils.JsonUtil;

import jakarta.json.JsonString;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class FileLoader {
    private static final Logger logger = Logger.getLogger(FileLoader.class.getName());

    private static String base() {
        return System.getProperty("multitool.base.dir", "/tmp");
    }

    private static String docs() {
        return System.getProperty("multitool.docs.subdir");
    }

    public static String commons() {
        return System.getProperty("multitool.commons.subdir");
    }

    public static String dir(String dir) {
        return Paths.get(base(), dir).toAbsolutePath().toString();
    }

    public static Map<String, String> loadDocumentsMap(String dir) throws IOException {
        var path = Paths.get(base(), dir, "dokumenty.json");
        if (Files.isReadable(path)) {
            var json = JsonUtil.deserialize(Files.readAllBytes(path));
            return json.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, val -> ((JsonString) val.getValue()).getString()));
        } else {
            logger.log(Level.WARNING, "documents file '" + path.toAbsolutePath() + "' doesn't exist!");
            return Map.of();
        }
    }

    public static Map<String, String[]> loadTransforms(String dir) throws IOException {
        var path = Paths.get(base(), dir, "transformaty.json");
        if (Files.isReadable(path)) {
            var json = JsonUtil.deserialize(Files.readAllBytes(path));
            return json.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, val -> new String[]{val.getValue().asJsonObject().getString("p"), val.getValue().asJsonObject().getString("r")}));
        } else {
            logger.log(Level.WARNING, "transformations file '" + path.toAbsolutePath() + "' doesn't exist!");
            return Map.of();
        }
    }

    public static byte[] loadPdf(String dir, String file) throws IOException {
        var path = Paths.get(base(), dir, docs(), file);
        return Files.readAllBytes(path);
    }

    public static String loadTemplate(String dir, String file) throws IOException {
        var path = Paths.get(base(), dir, file);
        return Files.readString(path);
    }
}
