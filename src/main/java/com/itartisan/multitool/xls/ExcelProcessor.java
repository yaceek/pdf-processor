package com.itartisan.multitool.xls;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.CellUtil;

import jakarta.json.JsonObject;
import jakarta.json.JsonString;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExcelProcessor {
    public static byte[] process(JsonObject json) throws IOException {
        var jsonSheet = json.getJsonObject("sheet");
        var jsonSheetName = jsonSheet.getString("name", null);

        var workbook = new HSSFWorkbook();
        var sheet = jsonSheetName != null ? workbook.createSheet(jsonSheetName) : workbook.createSheet();
        var fonts = prepareFontsMap(workbook);

        var styles = new HashMap<CellRangeAddress, Map<CellPropertyType, Object>>();
        var numeric = new ArrayList<CellRangeAddress>();
        for (var s : jsonSheet.getJsonArray("styles")) {
            var coord = ((JsonObject) s).getString("c");
            var sobj = ((JsonObject) s).getJsonObject("s");

            var params = extractStyle(workbook, sobj, fonts);
            var range = CellRangeAddress.valueOf(coord);
            styles.put(range, params);
        }

        if (jsonSheet.containsKey("numeric")) {
            for (var n : jsonSheet.getJsonArray("numeric")) {
                numeric.add(CellRangeAddress.valueOf(((JsonString) n).getString()));
            }
        }

        for (var c : jsonSheet.getJsonArray("cells")) {
            var coord = ((JsonObject) c).getString("c");
            var val = ((JsonObject) c).getString("v");
            var s = ((JsonObject) c).getJsonObject("s");

            var style = s != null ? processStyle(workbook, s, fonts) : null;
            var cr = new CellReference(coord);

            var row = CellUtil.getRow(cr.getRow(), sheet);
            var cell = CellUtil.getCell(row, cr.getCol());
            if (val.startsWith("=")) {
                cell.setCellFormula(val.substring(1));
            } else if (!val.isEmpty()) {
                var n = false;
                for (var r : numeric) {
                    if (r.isInRange(cr)) {
                        n = true;
                        break;
                    }
                }
                if (n) {
                    cell.setCellValue(Double.parseDouble(val));
                } else {
                    cell.setCellValue(val);
                }
            }

            if (style != null) {
                cell.setCellStyle(style);
            }
        }

        for (var entry : styles.entrySet()) {
            var range = entry.getKey();
            for (var c : range) {
                var row = CellUtil.getRow(c.getRow(), sheet);
                var cell = CellUtil.getCell(row, c.getColumn());
                CellUtil.setCellStylePropertiesEnum(cell, entry.getValue());
            }
        }

        for (var a : jsonSheet.getJsonArray("auto")) {
            sheet.autoSizeColumn(CellReference.convertColStringToIndex(((JsonString) a).getString()));
        }

        sheet.setDisplayGridlines(jsonSheet.getBoolean("grid", true));

        try (var out = new ByteArrayOutputStream()) {
            workbook.write(out);
            return out.toByteArray();
        }
    }

    private static Map<String, Font> prepareFontsMap(Workbook workbook) {
        var fonts = new HashMap<String, Font>();
        var fi = workbook.createFont();
        var fb = workbook.createFont();
        var fbi = workbook.createFont();
        fi.setItalic(true);
        fb.setBold(true);
        fbi.setItalic(true);
        fbi.setBold(true);
        fonts.put("B", fb);
        fonts.put("I", fi);
        fonts.put("BI", fbi);
        fonts.put("IB", fbi);

        return fonts;
    }

    private static Map<CellPropertyType, Object> extractStyle(Workbook workbook, JsonObject sobj, Map<String, Font> fonts) {
        var f = sobj.getString("f", ""); // font
        var d = sobj.getString("d", ""); // data format
        var ao = sobj.getJsonObject("a");
        var bo = sobj.getJsonObject("b");

        var map = new HashMap<CellPropertyType, Object>();
        if (!f.isEmpty()) {
            map.put(CellPropertyType.FONT, fonts.get(f).getIndex());
        }
        if (!d.isEmpty()) {
            var fmt = workbook.createDataFormat();
            map.put(CellPropertyType.DATA_FORMAT, fmt.getFormat(d));
        }
        if (ao != null) {
            var h = ao.getString("h", "");
            switch (h) {
                case "left":
                    map.put(CellPropertyType.ALIGNMENT, HorizontalAlignment.LEFT);
                    break;
                case "center":
                    map.put(CellPropertyType.ALIGNMENT, HorizontalAlignment.CENTER);
                    break;
                case "right":
                    map.put(CellPropertyType.ALIGNMENT, HorizontalAlignment.RIGHT);
                    break;
            }
            var v = ao.getString("v", "");
            switch (v) {
                case "top":
                    map.put(CellPropertyType.VERTICAL_ALIGNMENT, VerticalAlignment.TOP);
                    break;
                case "center":
                    map.put(CellPropertyType.VERTICAL_ALIGNMENT, VerticalAlignment.CENTER);
                    break;
                case "bottom":
                    map.put(CellPropertyType.VERTICAL_ALIGNMENT, VerticalAlignment.BOTTOM);
                    break;
            }
            if (ao.getString("w", "").equals("true")) {
                map.put(CellPropertyType.WRAP_TEXT, true);
            }
        }
        if (bo != null) {
            var t = bo.getString("t", "");
            if (t.equals("thin")) {
                map.put(CellPropertyType.BORDER_TOP, BorderStyle.THIN);
            }
            var b = bo.getString("b", "");
            if (b.equals("thin")) {
                map.put(CellPropertyType.BORDER_BOTTOM, BorderStyle.THIN);
            }
            var l = bo.getString("l", "");
            if (l.equals("thin")) {
                map.put(CellPropertyType.BORDER_LEFT, BorderStyle.THIN);
            }
            var r = bo.getString("r", "");
            if (r.equals("thin")) {
                map.put(CellPropertyType.BORDER_RIGHT, BorderStyle.THIN);
            }
        }

        return map;
    }

    private static CellStyle processStyle(Workbook workbook, JsonObject sobj, Map<String, Font> fonts) {
        var f = sobj.getString("f", ""); // font
        var d = sobj.getString("d", ""); // data format
        var ao = sobj.getJsonObject("a");
        var bo = sobj.getJsonObject("b");

        var style = workbook.createCellStyle();

        if (!f.isEmpty()) {
            style.setFont(fonts.get(f));
        }
        if (!d.isEmpty()) {
            var fmt = workbook.createDataFormat();
            style.setDataFormat(fmt.getFormat(d));
        }
        if (ao != null) {
            var h = ao.getString("h", "");
            switch (h) {
                case "left":
                    style.setAlignment(HorizontalAlignment.LEFT);
                    break;
                case "center":
                    style.setAlignment(HorizontalAlignment.CENTER);
                    break;
                case "right":
                    style.setAlignment(HorizontalAlignment.RIGHT);
                    break;
            }
            var v = ao.getString("v", "");
            switch (v) {
                case "top":
                    style.setVerticalAlignment(VerticalAlignment.TOP);
                    break;
                case "center":
                    style.setVerticalAlignment(VerticalAlignment.CENTER);
                    break;
                case "bottom":
                    style.setVerticalAlignment(VerticalAlignment.BOTTOM);
                    break;
            }
            if (ao.getString("w", "").equals("true")) {
                style.setWrapText(true);
            }
        }
        if (bo != null) {
            var t = bo.getString("t", "");
            if (t.equals("thin")) {
                style.setBorderTop(BorderStyle.THIN);
            }
            var b = bo.getString("b", "");
            if (b.equals("thin")) {
                style.setBorderBottom(BorderStyle.THIN);
            }
            var l = bo.getString("l", "");
            if (l.equals("thin")) {
                style.setBorderLeft(BorderStyle.THIN);
            }
            var r = bo.getString("r", "");
            if (r.equals("thin")) {
                style.setBorderRight(BorderStyle.THIN);
            }
        }
        return style;
    }
}
