package com.itartisan.multitool.rest.filters;

import com.itartisan.multitool.rest.utils.JWTSecured;
import com.itartisan.multitool.rest.utils.ResponseUtil;

import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;
import jakarta.ws.rs.ext.Provider;

import java.security.Principal;
import java.util.Objects;

@Provider
@Priority(Priorities.AUTHENTICATION)
@JWTSecured
public class AuthRequestFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext requestContext) {
        var authorization = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (authorization != null && authorization.startsWith("Bearer ")) {
            var token = authorization.substring(7);
            if (Objects.equals(token, System.getProperty("multitool.secret.key"))) {
                requestContext.setSecurityContext(new SecurityContext() {
                    @Override
                    public Principal getUserPrincipal() {
                        return () -> "Authorized";
                    }

                    @Override
                    public boolean isUserInRole(String role) {
                        return false;
                    }

                    @Override
                    public boolean isSecure() {
                        return requestContext.getSecurityContext().isSecure();
                    }

                    @Override
                    public String getAuthenticationScheme() {
                        return "Bearer";
                    }
                });
            } else {
                requestContext.abortWith(ResponseUtil.prepareError(Response.Status.UNAUTHORIZED, "user account removed or locked"));
            }
        } else {
            requestContext.abortWith(ResponseUtil.prepareError(Response.Status.UNAUTHORIZED, "no Authorization header was found"));
        }
    }
}
