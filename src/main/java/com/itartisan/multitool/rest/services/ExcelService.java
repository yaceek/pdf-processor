package com.itartisan.multitool.rest.services;

import com.itartisan.multitool.rest.utils.JWTSecured;
import com.itartisan.multitool.rest.utils.JsonUtil;
import com.itartisan.multitool.rest.utils.ResponseUtil;
import com.itartisan.multitool.xls.ExcelProcessor;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/excels")
public class ExcelService {
    private static final Logger logger = Logger.getLogger(ExcelService.class.getName());

    @POST
    @JWTSecured
    @Path("/process")
    @Produces("application/json; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response process(@Context SecurityContext ctx, String payload) {
        long started = System.currentTimeMillis();
        Response response;
        try {
            logger.info(">> process... payload.length()='" + payload.length() + "'");
            if (System.getProperty("multitool.excel.dump.enabled", "false").equals("true")) {
                Files.writeString(Paths.get(System.getProperty("multitool.excel.dump.path"), "dump_" + started + ".json"), payload);
            }
            var json = JsonUtil.deserialize(payload);
            var data = ExcelProcessor.process(json);
            return ResponseUtil.prepareResponse(Base64.getEncoder().encodeToString(data));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "caught exception...", ex);
            response = ResponseUtil.prepareError(Response.Status.INTERNAL_SERVER_ERROR, ex);
        }
        logger.info("<< process... processing time='" + (System.currentTimeMillis() - started) + "ms'");
        return response;
    }
}
