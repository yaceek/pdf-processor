package com.itartisan.multitool.rest.services;

import com.itartisan.multitool.rest.utils.JWTSecured;
import com.itartisan.multitool.rest.utils.JsonUtil;
import com.itartisan.multitool.rest.utils.ResponseUtil;
import com.itartisan.multitool.xls.ExcelProcessor;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/excels")
public class ExcelService {
    private static final Logger logger = Logger.getLogger(ExcelService.class.getName());

    @POST
    @JWTSecured
    @Path("/process")
    @Produces("application/json; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response process(@Context SecurityContext ctx, String payload) {
        long started = System.currentTimeMillis();
        Response response;
        try {
            logger.info(">> process... payload.length()='" + payload.length() + "'");
            if(System.getProperty("multitool.excel.dump.enabled", "false").equals("true")) {
                Files.write(Paths.get(System.getProperty("multitool.excel.dump.path"), "dump_" + started + ".json"), payload.getBytes(StandardCharsets.UTF_8));
            }
            var json = JsonUtil.deserialize(payload);
            var data = ExcelProcessor.process(json);
            return ResponseUtil.prepareResponse(Base64.getEncoder().encodeToString(data));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "caught exception...", ex);
            response = ResponseUtil.prepareError(Response.Status.INTERNAL_SERVER_ERROR, ex);
        }
        logger.info("<< process... processing time='" + (System.currentTimeMillis() - started) + "ms'");
        return response;
    }
}
