package com.itartisan.multitool.rest.services;

import com.itartisan.multitool.rest.utils.ResponseUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/status")
public class StatusService {
    @GET
    @Path("/check")
    @Produces("application/json; charset=UTF-8")
    public Response check() {
        return ResponseUtil.prepareResponse();
    }
}
