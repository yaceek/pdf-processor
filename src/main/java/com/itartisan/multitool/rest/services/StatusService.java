package com.itartisan.multitool.rest.services;

import com.itartisan.multitool.rest.utils.ResponseUtil;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;

@Path("/status")
public class StatusService {
    @GET
    @Path("/check")
    @Produces("application/json; charset=UTF-8")
    public Response check() {
        return ResponseUtil.prepareResponse();
    }
}
