package com.itartisan.multitool.rest.services;

import com.itartisan.multitool.pdf.loaders.FileLoader;
import com.itartisan.multitool.pdf.processors.PdfMergerProcessor;
import com.itartisan.multitool.pdf.processors.PdfPolicyProcessor;
import com.itartisan.multitool.pdf.processors.PdfSecurityProcessor;
import com.itartisan.multitool.pdf.processors.PdfWatermarkProcessor;
import com.itartisan.multitool.rest.utils.JWTSecured;
import com.itartisan.multitool.rest.utils.JsonUtil;
import com.itartisan.multitool.rest.utils.ResponseUtil;
import org.apache.commons.lang3.StringUtils;

import jakarta.json.*;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/documents")
public class DocumentService {
    private static final Logger logger = Logger.getLogger(DocumentService.class.getName());

    @GET
    @Path("/pdf")
    @Produces("application/pdf")
    public Response getPdf(@QueryParam("folder") String folder, @QueryParam("id") String id, @QueryParam("mode") String mode) {
        try {
            if (StringUtils.isEmpty(folder) || StringUtils.isEmpty(id) || (!Objects.equals(mode, "inline") && !Objects.equals(mode, "attachment"))) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            var docsmap = FileLoader.loadDocumentsMap(folder);
            var doc = docsmap.get(id);
            var pdf = FileLoader.loadPdf(folder, doc);
            return Response.ok(pdf).header("Content-Disposition", mode + "; filename=\"" + doc + "\"").build();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "caught exception...", ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @JWTSecured
    @Path("/process")
    @Produces("application/json; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response process(@Context SecurityContext ctx, String payload) {
        var timestamp = System.currentTimeMillis();
        Response response;
        try {
            if (System.getProperty("multitool.pdf.dump.enabled", "false").equals("true")) {
                Files.writeString(Paths.get(System.getProperty("multitool.pdf.dump.path"), "dump_" + timestamp + ".json"), payload);
            }

            var json = JsonUtil.deserialize(payload);
            var folder = json.getString("folder");
            var docs = json.getJsonArray("documents").stream().map(val -> ((JsonString) val).getString()).toList();
            var parameters = Optional.ofNullable(json.getJsonObject("parameters")).orElse(JsonObject.EMPTY_JSON_OBJECT).entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, val -> JsonUtil.extractString(val.getValue())));
            var attachments = Optional.ofNullable(json.getJsonArray("attachments")).orElse(JsonArray.EMPTY_JSON_ARRAY).stream().map(val -> ((JsonString) val).getString()).collect(Collectors.toList());
            var prepared = Optional.ofNullable(json.getJsonArray("prepared")).orElse(JsonArray.EMPTY_JSON_ARRAY).stream().map(val -> Base64.getDecoder().decode(((JsonString) val).getString())).toList();
            var demo = Objects.equals(json.getString("version", "prod"), "demo");

            var password = json.containsKey("password") ? json.getString("password") : null;

            var commonmap = FileLoader.loadDocumentsMap(FileLoader.commons());
            var docsmap = FileLoader.loadDocumentsMap(folder);
            var transmap = FileLoader.loadTransforms(folder);

            var sources = new ArrayList<byte[]>();
            for (var doc : docs) {
                if (doc.startsWith("_")) {
                    sources.add(prepared.get(Integer.parseInt(doc.substring(1))));
                } else if (doc.startsWith("common_")) {
                    var d = commonmap.get(doc.substring(7));
                    if (d != null) {
                        if (d.endsWith(".pdf")) {
                            sources.add(FileLoader.loadPdf(FileLoader.commons(), d));
                        } else {
                            var pdf = PdfPolicyProcessor.process(FileLoader.commons(), d, parameters, transmap, attachments);
                            if (demo) {
                                pdf = PdfWatermarkProcessor.process(pdf);
                            }
                            sources.add(pdf);
                        }
                    } else {
                        logger.log(Level.WARNING, "document '" + doc + "' not found in documents map");
                    }
                } else {
                    var d = docsmap.get(doc);
                    if (d != null) {
                        if (d.endsWith(".pdf")) {
                            sources.add(FileLoader.loadPdf(folder, d));
                        } else {
                            var pdf = PdfPolicyProcessor.process(folder, d, parameters, transmap, attachments);
                            if (demo) {
                                pdf = PdfWatermarkProcessor.process(pdf);
                            }
                            sources.add(pdf);
                        }
                    } else {
                        logger.log(Level.WARNING, "document '" + doc + "' not found in documents map");
                    }
                }
            }

            var merged = PdfMergerProcessor.merge(sources);
            if (password != null) {
                merged = PdfSecurityProcessor.process(merged, password);
            }

            if (System.getProperty("multitool.pdf.dump.enabled", "false").equals("true")) {
                Files.write(Paths.get(System.getProperty("multitool.pdf.dump.path"), "merged_" + timestamp + ".pdf"), merged);
            }

            return ResponseUtil.prepareResponse(Base64.getEncoder().encodeToString(merged));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "caught exception...", ex);
            response = ResponseUtil.prepareError(Response.Status.INTERNAL_SERVER_ERROR, ex);
        }
        return response;
    }

    @POST
    @JWTSecured
    @Path("/merge")
    @Produces("application/json; charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response merge(@Context SecurityContext ctx, String payload) {
        Response response;
        try {
            var json = JsonUtil.deserialize(payload);
            var docs = json.getJsonArray("docs");
            var list = docs.stream().map(val -> Base64.getDecoder().decode(((JsonString) val).getString())).collect(Collectors.toList());
            var password = json.containsKey("password") ? json.getString("password") : null;

            var merged = PdfMergerProcessor.merge(list);
            if (password != null) {
                merged = PdfSecurityProcessor.process(merged, password);
            }

            return ResponseUtil.prepareResponse(Base64.getEncoder().encodeToString(merged));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "caught exception...", ex);
            response = ResponseUtil.prepareError(Response.Status.INTERNAL_SERVER_ERROR, ex);
        }
        return response;
    }
}
