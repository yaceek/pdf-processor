package com.itartisan.multitool.rest.utils;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;

public class JsonUtil {
    public static JsonObject deserialize(byte[] data) {
        var reader = Json.createReader(new StringReader(new String(data, StandardCharsets.UTF_8)));
        return reader.readObject();
    }

    public static JsonObject deserialize(String str) {
        var reader = Json.createReader(new StringReader(str));
        return reader.readObject();
    }

    public static String extractString(JsonValue val) {
        switch (val.getValueType()) {
            case NULL:
                return "";
            case STRING:
                return ((JsonString)val).getString();
            case NUMBER:
            case TRUE:
            case FALSE:
                return val.toString();
        }
        return "";
    }
}
