package com.itartisan.multitool.rest.utils;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonString;
import jakarta.json.JsonValue;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;

public class JsonUtil {
    public static JsonObject deserialize(byte[] data) {
        var reader = Json.createReader(new StringReader(new String(data, StandardCharsets.UTF_8)));
        return reader.readObject();
    }

    public static JsonObject deserialize(String str) {
        var reader = Json.createReader(new StringReader(str));
        return reader.readObject();
    }

    public static String extractString(JsonValue val) {
        return switch (val.getValueType()) {
            case STRING -> ((JsonString) val).getString();
            case NUMBER, TRUE, FALSE -> val.toString();
            default -> "";
        };
    }
}
