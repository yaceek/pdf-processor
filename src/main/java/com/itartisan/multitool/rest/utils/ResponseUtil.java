package com.itartisan.multitool.rest.utils;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;

public class ResponseUtil {
    public static Response prepareResponse() {
        return Response.ok().entity(Json.createObjectBuilder().add("status", "OK").build().toString()).build();
    }

    public static Response prepareResponse(JsonObject payload) {
        return Response.ok().entity(Json.createObjectBuilder().add("status", "OK").add("payload", payload).build().toString()).build();
    }

    public static Response prepareResponse(String payload) {
        return Response.ok().entity(Json.createObjectBuilder().add("status", "OK").add("payload", payload).build().toString()).build();
    }

    public static Response prepareError(Response.Status status, String message) {
        return Response.status(status).entity(Json.createObjectBuilder().add("status", "ERROR").add("message", message).build().toString()).build();
    }

    public static Response prepareError(Response.Status status, Exception e) {
        return prepareError(status, e.getMessage() != null ? e.getMessage() : e.getClass().getName());
    }
}
