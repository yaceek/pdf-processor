package com.itartisan.multitool.tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;

public class Runner {
    public static void main(String[] args) throws Exception {
        try (var workbook = new HSSFWorkbook()) {
            var sheet = workbook.createSheet();

            for (var i = 0; i < 100; i++) {
                var row = sheet.createRow(i);
                for (var j = 0; j < 100; j++) {
                    var cell = row.createCell(j);
                    cell.setCellValue(RandomStringUtils.secure().nextAlphanumeric(100));
                }
            }

            workbook.write(new File("target/test.xls"));
        }
    }
}
