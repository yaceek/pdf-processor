package com.itartisan.multitool.tests;

import com.itartisan.multitool.pdf.processors.PdfSecurityProcessor;
import com.itartisan.multitool.pdf.processors.PdfWatermarkProcessor;
import com.itartisan.multitool.pdf.tags.QRCodeTagCssApplierFactory;
import com.itartisan.multitool.pdf.tags.QRCodeTagWorkerFactory;
import com.itartisan.multitool.rest.utils.JsonUtil;
import com.itartisan.multitool.xls.ExcelProcessor;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.junit.jupiter.api.Test;

import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonString;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


class SomeTests {
    @Test
    void aaaTest() throws Exception {
        var payload = "{\"folder\":\"HELVETIA\\/serwisAutoEU\\/gaphelvetia\",\"documents\":[\"PL_polisa\"],\"parameters\":{\"IDKontrakt\":\"87\",\"IDWniosek\":\"1\",\"IDSprzedawca\":\"1017\",\"IDFirma\":\"957\",\"DataZgloszenia\":\"2020-06-24\",\"DataWystawienia\":\"2020-06-24\",\"DataModyfikacji\":\"2020-06-24 22:07:40\",\"KodSprzedawcy\":\"GAP660\",\"KodHandlowca\":\"\",\"Dostepnosc\":\"Z\",\"Usun\":\"N\",\"Akceptacja\":\"T\",\"Miejsce\":\"Warszawa\",\"IDMarkaModel\":\"931\",\"MarkaInna\":\"BMW\",\"ModelInny\":\"335\",\"NrRejestracyjny\":\"1111111\",\"NrNadwozia\":\"11111111111111111\",\"PojemnoscSilnika\":\"1111\",\"RokProdukcji\":\"2020\",\"Data1Rejestracji\":\"2020-01-11\",\"DataZakupu\":\"2020-06-24\",\"WartoscPojazdu\":\"111111\",\"WariantUbezp\":\"1\",\"TaryfikatorNr\":\"1\",\"TaryfikatorPoz\":\"23\",\"UOkres\":\"48\",\"UOkresOd\":\"2020-06-24\",\"UOkresDo\":\"2024-06-23\",\"Skladka\":\"2601.00\",\"FormaPlatnosci\":\"G\",\"Prowizja\":\"920.00\",\"ProwizjaSprzedawcy\":\"0.00\",\"IDPaczka\":\"\",\"DataPlatnosci\":\"\",\"DaneOsobowe\":\"N\",\"KontaktElek\":\"T\",\"Imie\":\"111111111\",\"Nazwisko\":\"11111111\",\"OsobaPesel\":\"11111111111\",\"OsobaNip\":\"\",\"OsobaKrs\":\"\",\"KodPocztowy\":\"11-111\",\"Miejscowosc\":\"11111111\",\"Ulica\":\"11111111111\",\"NrDomu\":\"11\",\"NrLokalu\":\"11\",\"Telefon\":\"\",\"Email\":\"\",\"DataUr\":\"\",\"MiejsceUr\":\"\",\"OjciecImie\":\"\",\"MatkaImie\":\"\",\"MatkaNazwiskoRodowe\":\"\",\"OsobaJakWyzej\":\"T\",\"WImie\":\"111111111\",\"WNazwisko\":\"11111111\",\"WOsobaPesel\":\"11111111111\",\"WOsobaNip\":\"\",\"WOsobaKrs\":\"\",\"WKodPocztowy\":\"11-111\",\"WMiejscowosc\":\"11111111\",\"WUlica\":\"11111111111\",\"WNrDomu\":\"11\",\"WNrLokalu\":\"11\",\"WTelefon\":\"\",\"WEmail\":\"\",\"WDataUr\":\"\",\"WMiejsceUr\":\"\",\"WOjciecImie\":\"\",\"WMatkaImie\":\"\",\"WMatkaNazwiskoRodowe\":\"\",\"Ubezpieczyciel\":\"HELVETIA\",\"PapierFR\":\"T\",\"PapierDR\":\"T\",\"PapierUB\":\"T\",\"PapierAC\":\"T\",\"JestUB\":\"N\",\"JestFR\":\"N\",\"JestDR\":\"N\",\"JestAC\":\"N\",\"NrDecyzjiZU\":\"\",\"DataDecyzjiZU\":\"\",\"Podpis\":\"N\",\"PodpisWagas\":\"\",\"Mark2\":\"0\",\"SymbolKraju\":\"PL\",\"SymbolWaluty\":\"PLN\",\"Dokument\":\"1\",\"PapierO1\":\"N\",\"PapierO2\":\"N\",\"JestO1\":\"N\",\"JestO2\":\"N\",\"Osobowosc\":\"F\",\"WOsobowosc\":\"F\",\"Leasing\":\"\",\"NrPolisyAC\":\"\",\"UOkresOdAC\":\"\",\"valSumaUbezp\":\"0\",\"SumaUbezp\":\"84800\",\"PapierRO\":\"N\",\"JestRO\":\"N\",\"FormatDrukuNr\":\"3\",\"PlatnoscBN\":\"\",\"Taxi\":\"2\",\"NaukaJazdy\":\"2\",\"RejestracjaRP\":\"1\",\"PolisaAC\":\"1\",\"Wynajem\":\"2\",\"MamZgodeKE\":\"\",\"BezZalacznikow\":\"N\",\"PapierKE\":\"N\",\"JestKE\":\"N\",\"NrAkceptacji\":\"GP100001TEST\",\"TelefonCLS\":\"22\\/831 09 03\",\"KontaktElekTxt\":\"Wyra\\u017cam zgod\\u0119\",\"PodpisAgenta\":\"T\",\"PodpisAgentaSpecjalnego\":\"podpis_wagas.jpg\",\"PodpisPodwojny\":\"T\",\"ZaPosrednictwemTxt\":\"WAGAS S.A., 00-236 Warszawa, \\u015awi\\u0119tojerska 5\\/7\",\"UIDPolisy\":\"ce51eb0adbfe285a46b5f62470be1bd8\",\"WariantUbezpTxt\":\"GAP Fakturowy\",\"LabelkaPesel\":\"PESEL\",\"PlatnoscGotowka\":\"T\"},\"attachments\":[\"faktura zakupu pojazdu\",\"dow\\u00f3d rejestracyjny pojazdu\",\"polisa AC pojazdu\"],\"version\":\"demo\"}";
        var json = JsonUtil.deserialize(payload);
        var folder = json.getString("folder");
        var docs = json.getJsonArray("documents").stream().map(val -> ((JsonString) val).getString()).toList();
        var parameters = Optional.ofNullable(json.getJsonObject("parameters")).orElse(JsonObject.EMPTY_JSON_OBJECT).entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, val -> JsonUtil.extractString(val.getValue())));
        var attachments = Optional.ofNullable(json.getJsonArray("attachments")).orElse(JsonArray.EMPTY_JSON_ARRAY).stream().map(val -> ((JsonString) val).getString()).toList();
        var prepared = Optional.ofNullable(json.getJsonArray("prepared")).orElse(JsonArray.EMPTY_JSON_ARRAY).stream().map(val -> Base64.getDecoder().decode(((JsonString) val).getString())).toList();
        var demo = Objects.equals(json.getString("version", "prod"), "demo");
    }

    @Test
    void encryptTest() throws Exception {
        var sec = PdfSecurityProcessor.process(Files.readAllBytes(Path.of(this.getClass().getResource("/printable_feet_measurer.pdf").toURI())), "haselko");
        Files.write(Paths.get("target", "encrypted.pdf"), sec);
    }

    @Test
    void policyHtmlToPdfTest() throws Exception {
        var tgt = File.createTempFile("polisa_", ".pdf");
        HtmlConverter.convertToPdf(new File(this.getClass().getResource("/bmwPL/gapbmw/polisa2.html").getFile()), tgt);
        System.out.println(tgt.getAbsolutePath());
    }

    @Test
    void policyHtmlToPdfWatermarkedTest() throws Exception {
        var tgt1 = File.createTempFile("polisa_", ".pdf");
        var tgt2 = File.createTempFile("polisa_watermarked", ".pdf");

        var properties = new ConverterProperties()
                .setTagWorkerFactory(new QRCodeTagWorkerFactory())
                .setCssApplierFactory(new QRCodeTagCssApplierFactory());

        HtmlConverter.convertToPdf(new File(this.getClass().getResource("/rodo.html").getFile()), tgt1, properties);
        Files.write(tgt2.toPath(), PdfWatermarkProcessor.process(Files.readAllBytes(tgt1.toPath())));
        System.out.println(tgt1.getAbsolutePath());
        System.out.println(tgt2.getAbsolutePath());
    }

    @Test
    void splitTest() {
        System.out.println(Arrays.toString(StringUtils.split("{SumaUbezpTxt}", "|{}")));
        System.out.println(Arrays.toString(StringUtils.split("{SumaUbezpTxt|data}", "|{}")));
    }

    @Test
    void transformsTest() {
        System.out.println("2018-07-04".replaceAll("(\\d+)-(\\d+)-(\\d+)", "$3-$2-$1"));
        System.out.println("12345678901234567890".replaceAll("[0-9](?=(?:[0-9]{3})+(?![0-9]))", "$0 "));
        System.out.println("".replaceAll("^$", "-"));
        System.out.println("aaa".replaceAll("^$", "-"));
    }

    @Test
    void b64Test() throws IOException {
        var enc = "JVBERi0xLjcKCjEgMCBvYmogICUgZW50cnkgcG9pbnQKPDwKICAvVHlwZSAvQ2F0YWxvZwogIC9QYWdlcyAyIDAgUgo+PgplbmRvYmoKCjIgMCBvYmoKPDwKICAvVHlwZSAvUGFnZXMKICAvTWVkaWFCb3ggWyAwIDAgMjAwIDIwMCBdCiAgL0NvdW50IDEKICAvS2lkcyBbIDMgMCBSIF0KPj4KZW5kb2JqCgozIDAgb2JqCjw8CiAgL1R5cGUgL1BhZ2UKICAvUGFyZW50IDIgMCBSCiAgL1Jlc291cmNlcyA8PAogICAgL0ZvbnQgPDwKICAgICAgL0YxIDQgMCBSIAogICAgPj4KICA+PgogIC9Db250ZW50cyA1IDAgUgo+PgplbmRvYmoKCjQgMCBvYmoKPDwKICAvVHlwZSAvRm9udAogIC9TdWJ0eXBlIC9UeXBlMQogIC9CYXNlRm9udCAvVGltZXMtUm9tYW4KPj4KZW5kb2JqCgo1IDAgb2JqICAlIHBhZ2UgY29udGVudAo8PAogIC9MZW5ndGggNDQKPj4Kc3RyZWFtCkJUCjcwIDUwIFRECi9GMSAxMiBUZgooSGVsbG8sIHdvcmxkISkgVGoKRVQKZW5kc3RyZWFtCmVuZG9iagoKeHJlZgowIDYKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAwMDEwIDAwMDAwIG4gCjAwMDAwMDAwNzkgMDAwMDAgbiAKMDAwMDAwMDE3MyAwMDAwMCBuIAowMDAwMDAwMzAxIDAwMDAwIG4gCjAwMDAwMDAzODAgMDAwMDAgbiAKdHJhaWxlcgo8PAogIC9TaXplIDYKICAvUm9vdCAxIDAgUgo+PgpzdGFydHhyZWYKNDkyCiUlRU9G";
        byte[] decoded = Base64.getDecoder().decode(enc);
        Files.write(Paths.get("target", "decoded.pdf"), decoded);
    }

    @Test
    void eqTest() {
        var parameters = Map.of("parametr_1", "123");
        var match = "{rowne_parametr_1_123}";
        var mark = StringUtils.split(match, "|{}");

        var eq = mark[0].startsWith("rowne_");
        var val = parameters.get(mark[0]);
        var cmp = mark[0].substring(mark[0].lastIndexOf('_') + 1);

        val = eq ? parameters.get(mark[0].substring(6, mark[0].lastIndexOf('_'))) : val;
        if (eq && !val.equals(cmp)) {
            System.out.println("hide!");
        } else {
            System.out.println("nope");
        }
    }

    @Test
    void xlsTest() throws IOException {
        try (var workbook = new HSSFWorkbook()) {
            var sheet = workbook.createSheet();

            for (var i = 0; i < 5000; i++) {
                var row = sheet.createRow(i);
                for (var j = 0; j < 100; j++) {
                    var cell = row.createCell(j);
                    cell.setCellValue(RandomStringUtils.secure().nextAlphanumeric(100));
                }
            }

            workbook.write(Files.newOutputStream(Paths.get("target", "xls_test.xls")));
        }
    }

    @Test
    void xlsxTest() throws IOException {
        try (var workbook = new SXSSFWorkbook(100)) {
            var sheet = workbook.createSheet();

            for (var i = 0; i < 5000; i++) {
                var row = sheet.createRow(i);
                for (var j = 0; j < 100; j++) {
                    var cell = row.createCell(j);
                    cell.setCellValue(RandomStringUtils.secure().nextAlphanumeric(100));
                }
            }

            workbook.write(Files.newOutputStream(Paths.get("target", "xlsx_test.xlsx")));
        }
    }

    @Test
    void json2xlsTest() throws IOException, URISyntaxException {
        var json = JsonUtil.deserialize(Files.readAllBytes(Paths.get(this.getClass().getResource("/Raport_nowy.json").toURI())));
        var bytes = ExcelProcessor.process(json);
        Files.write(Paths.get("target", "json2xls_report.xls"), bytes);
    }
}
